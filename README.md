# NESem

A rather silly-coded NES emulator written in C++, using a buggy core I first wrote years ago.

[The development has been narrated in detail at my site](https://kdrnic.github.io/index_nesem1.html) and the irony of writing a github page about a bitbucket
repo hasn't been lost on me.

# Building

This depends on Allegro 4, which should be available as a package on most Unix-y systems or as a zip for Windows.

Then,

    make regular

will build the emulator.

There is also

    make profile
    make debug

For profiling and debugging, obviously.

# Using the emulator

It can be ran from command line like

    game rom.nes

otherwise it will boot up to a file selection dialog (no mouse input).

A file called kdrnes.cfg may be present at the directory, it is typical cfg format and you need to create a "windows" section that may have fields
"screenw" and "screenh" which are obvious.

## Keys

Keys are currently hardcoded to A-S for select-start, X-C for B-A, arrow keys for DPAD.

Spacebar activates fast forward.

F1 and F2 for storing and loading savestates. There are 9 slots, selected through 1-9 num row (but not numpad) keys.

Some games, however, will crash. Feel free to investigate why, and then tell me.
