#define PAL_OFFSET 1
#define PAL_FROMNES(x) ((x)+PAL_OFFSET)

void RenderScanline(NESMemory &nes_mem, uint8_t scr_y, BITMAP *bmp);
void CreateNESPalette(PALETTE pal);
