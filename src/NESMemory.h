class NESMemory : public Memory
{
	public:
	uint8_t ram[0x800];
	uint8_t ppu[8];
	uint8_t apu[0x18];
	uint8_t apu_w[0x18];
	uint8_t sram[0x2000];
	uint8_t *rom;
	uint8_t	*ines;
	uint8_t *chr;
	uint8_t oam[0x100];
	uint8_t vram[0x1000];
	uint8_t ppu_pal[0x20];
	uint16_t ppu_addr;
	uint8_t ppu_x, ppu_y, ppu_latch;
	uint8_t joy1, joy2, joy1_bit, joy2_bit, joy_latch;
	uint8_t unrom_bank;
	uint8_t chr_ram[16384];
	uint8_t open_bus;
	uint8_t cnrom_bank;
	uint8_t mmc_shift_bit;
	uint8_t mmc_shift_reg;
	uint8_t mmc_shift_cnt;
	uint8_t mmc_ctrl;
	uint8_t mmc_chr_bnk0, mmc_chr_bnk1;
	uint8_t mmc_prg_bank;
	uint8_t joy2_port;
	
	int dma_cycles;
	long ines_len;
	int
		prg_siz,
		chr_siz,
		mirror,
		has_sram,
		has_trainer,
		has_vram,
		mapper;
	
	apu_data apu_state;
	int apuCycles;
	uint16_t sampleBuffer[1024 * 128];
	uint16_t *ptrSampled, *ptrBuffered;
	
	char battery_fn[512];
	
	void SyncAPU(bool sync_regs = true);
	uint16_t *GetSamples(int n);
	int GetDMACycles();
	void Read(const MemoryByte &mb);
	uint8_t &GetPPU(uint16_t addr);
	void Write(const MemoryByte &mb);
	MemoryByte operator[](std::size_t address);
	void Savestate(FILE *f);
	void Loadstate(FILE *f);
	void LoadROM(const char *fn);
	
	NESMemory();
	~NESMemory();
};