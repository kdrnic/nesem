//Function to read an entire file, in either binary or text mode

#ifndef READFILE_H
#define READFILE_H

#ifdef __cplusplus
extern "C"{
#endif

long read_file(const char *filename, char **ptxt, char binary);

#ifdef __cplusplus
};
#endif

#endif
