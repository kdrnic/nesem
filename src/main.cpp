#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>

#include <allegro.h>

#include "6502.h"
#include "readfile.h"
#include "apu.h"
#include "wav.h"
#include "NESdefs.h"
#include "NESMemory.h"
#include "Render.h"

struct joy_input_cfg
{
	int a, b, select, start, up, down, left, right;
};

joy_input_cfg joy_input_default = {KEY_C, KEY_X, KEY_A, KEY_S, KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT};

uint8_t GetJoyInput(joy_input_cfg cfg)
{
	uint8_t v = 0;
	if(key[cfg.a]) v |= JOY_BUTTON_A;
	if(key[cfg.b]) v |= JOY_BUTTON_B;
	if(key[cfg.select]) v |= JOY_BUTTON_SELECT;
	if(key[cfg.start]) v |= JOY_BUTTON_START;
	if(key[cfg.up]) v |= JOY_BUTTON_UP;
	if(key[cfg.down]) v |= JOY_BUTTON_DOWN;
	if(key[cfg.left]) v |= JOY_BUTTON_LEFT;
	if(key[cfg.right]) v |= JOY_BUTTON_RIGHT;
	return v;
}

volatile unsigned int frame, last_second_frame, fps, last_second_rc, rcps, tick_t;
static void count_fps()
{
	fps = frame - last_second_frame;
	rcps = retrace_count - last_second_rc;
	last_second_frame = frame;
	last_second_rc = retrace_count;
}
END_OF_FUNCTION(count_fps);

static void tick()
{
	++tick_t;
}
END_OF_FUNCTION(tick);

#ifndef __DJGPP__
#define CFG_SECTION "windows"
#else
#define CFG_SECTION "dos"
#endif
int init_io()
{
	allegro_init();
	set_config_file("kdrnes.cfg");
	set_color_depth(8);
	
	#ifndef __DJGPP__
	if(set_gfx_mode(
		GFX_AUTODETECT_WINDOWED,
		get_config_int(CFG_SECTION, "screenw", 640),
		get_config_int(CFG_SECTION, "screenh", 480),
		0,
		0
	)){
	#else
	if(set_gfx_mode(
		GFX_AUTODETECT,
		get_config_int(CFG_SECTION, "screenw", 640),
		get_config_int(CFG_SECTION, "screenh", 480),
		0,
		0
	)){
	#endif
		allegro_exit();
		printf("Couldn't set graphics mode: \"%s\"\n", allegro_error);
		return 1;
	}
	
	install_keyboard();
	install_timer();
	install_sound(DIGI_AUTODETECT, MIDI_NONE, "");
	
	//install_keyboard will call setlocale on X systems -
	//see Allegro src/unix/xkeyboard.c -
	//which will change the decimal-point character for some countries,
	//in turn causing float to string operations to fail,
	//so we revert it
	setlocale(LC_NUMERIC, "C");
	
	LOCK_VARIABLE(fps);
	LOCK_VARIABLE(frame);
	LOCK_VARIABLE(last_second_frame);
	LOCK_VARIABLE(rcps);
	LOCK_VARIABLE(last_second_rc);
	LOCK_FUNCTION(count_fps);
	install_int(count_fps, 1000);
	
	LOCK_VARIABLE(tick_t);
	LOCK_FUNCTION(tick);
	install_int_ex(tick, BPS_TO_TIMER(60));
	
	set_display_switch_mode(SWITCH_PAUSE);
	
	set_window_title("kdrNES");
	
	set_volume(255, 255);
	set_hardware_volume(255, 255);
	set_volume_per_voice(0);
	
	return 0;
}

int main(int argc, char **argv)
{
	FILE *log_file = 0;
	char rom_file_buf[512] = {};
	const char *rom_file = 0;
	const char *wav_fn = 0;
	for(int i = 1; i < argc; i++){
		std::string arg(argv[i]);
		if(arg == "-log") log_file = stdout;
		if(arg.find("-")){
			if(arg.find(".nes") != std::string::npos) rom_file = argv[i];
			else if(arg.find(".log") != std::string::npos) log_file = fopen(arg.c_str(), "w");
			else if(arg.find(".wav") != std::string::npos) wav_fn = argv[i];
		}
	}
	
	//Init Allegro
	init_io();
	
	PALETTE pal = {{0,0,0}};
	CreateNESPalette(pal);
	set_palette(pal);
	RGB_MAP rgb_tbl;
	create_rgb_table(&rgb_tbl, pal, 0);
	rgb_map = &rgb_tbl;
	
	std::srand(std::time(0));
	
	if(!rom_file){
		if(!file_select_ex("Select ROM", rom_file_buf, "NES;/+a", sizeof(rom_file_buf) - 1, 0, 0)) return 0;
		if(exists(rom_file_buf)) rom_file = rom_file_buf;
		else return 0;
	}
	
	apu_precalc();
	
	NESMemory nes_mem;
	nes_mem.LoadROM(rom_file);
	
	Machine machine(nes_mem);
	machine.log_file = log_file;
	machine.Reset();

	BITMAP *canvas = create_bitmap(256, 240);
	clear_bitmap(canvas);
	
	int multiplier = SCREEN_H / 240;
	if(multiplier <= 0) multiplier = 1;
	
	int canvas_x = (SCREEN_W - 256 * multiplier) / 2;
	
	uint16_t *audio_buf = 0;
	int audio_buf_siz = 1024;
	AUDIOSTREAM *audio_stream = play_audio_stream(audio_buf_siz, 16, 0, 44100, 255, 128);
	
	int frame = 0;
	
	int save_slot = 0;
	
	uint16_t *wav_buf = 0;
	int wav_buf_siz = 0;
	if(wav_fn){
		wav_buf = (uint16_t *) malloc(audio_buf_siz * sizeof(uint16_t));
		wav_buf_siz = 1;
	}
	
	while(!key[KEY_ESC]){
		int frame_skip = key[KEY_SPACE] && (frame % 5 != 0);
		
		if(key[KEY_0]) save_slot = 0;
		if(key[KEY_1]) save_slot = 1;
		if(key[KEY_2]) save_slot = 2;
		if(key[KEY_3]) save_slot = 3;
		if(key[KEY_4]) save_slot = 4;
		if(key[KEY_5]) save_slot = 5;
		if(key[KEY_6]) save_slot = 6;
		if(key[KEY_7]) save_slot = 7;
		if(key[KEY_8]) save_slot = 8;
		if(key[KEY_9]) save_slot = 9;
		
		unsigned int cyclesCount = 0;
		for(int scanline = 0; scanline < 262; scanline++){
			switch(scanline){
				case 0 ... 240:
					RenderScanline(nes_mem, scanline, frame_skip ? 0 : canvas);
					break;
				case 241:
					if(nes_mem.ppu[PPUCTRL] & PPUCTRL_NMI) machine.NMI();
					nes_mem.ppu[PPUSTATUS] |= PPUSTATUS_VBLANK;
					
					if(key[KEY_F1]){
						char savfile[512], *svfptr;
						strcpy(savfile, rom_file);
						svfptr = savfile + strlen(savfile);
						while(*svfptr != '.') svfptr--;
						svfptr++; *svfptr = 's';
						svfptr++; *svfptr = 'v';
						svfptr++; *svfptr = '0' + save_slot;
						FILE *f = fopen(savfile, "wb");
						machine.Savestate(f);
						nes_mem.Savestate(f);
						fclose(f);
					}
					if(key[KEY_F2]){
						char savfile[512], *svfptr;
						strcpy(savfile, rom_file);
						svfptr = savfile + strlen(savfile);
						while(*svfptr != '.') svfptr--;
						svfptr++; *svfptr = 's';
						svfptr++; *svfptr = 'v';
						svfptr++; *svfptr = '0' + save_slot;
						FILE *f = fopen(savfile, "rb");
						machine.Loadstate(f);
						nes_mem.Loadstate(f);
						fclose(f);
					}
					
					break;
				case 261:
					nes_mem.ppu[PPUSTATUS] &= (255u - PPUSTATUS_VBLANK);
					nes_mem.ppu[PPUSTATUS] &= (255u - PPUSTATUS_SPR0HIT);
					break;
				default:
					break;
			}
			
			unsigned int scanlineCycles = 113 + (scanline % 3 > 0);
			while(cyclesCount < scanlineCycles){
				int cycles = machine.DoStep();
				cycles += nes_mem.GetDMACycles();
				cyclesCount += cycles;
				nes_mem.apuCycles += cycles;
				
				if(nes_mem.apu_state.irq_frame || nes_mem.apu_state.irq_dmc){
					machine.IRQ();
				}
			}
			cyclesCount -= scanlineCycles;
		}
		
		while(nes_mem.ptrSampled - nes_mem.ptrBuffered > audio_buf_siz * 8){
			nes_mem.GetSamples(audio_buf_siz);
		}
		if(audio_buf || (audio_buf = (uint16_t *) get_audio_stream_buffer(audio_stream))){
			uint16_t *buf = nes_mem.GetSamples(audio_buf_siz);
			if(buf){
				memcpy(audio_buf, buf, audio_buf_siz * 2);
				free_audio_stream_buffer(audio_stream);
				audio_buf = 0;
				
				if(wav_buf){
					memcpy(wav_buf + audio_buf_siz * (wav_buf_siz - 1), buf, audio_buf_siz * 2);
					wav_buf_siz++;
					wav_buf = (uint16_t *) realloc(wav_buf, audio_buf_siz * wav_buf_siz * 2);
				}
			}
		}
		
		if(!key[KEY_SPACE]){
			//vsync doesn't work well on inferior systems
			#ifdef __MINGW32__
			vsync();
			#else
			while(tick_t == 0){
				 vsync();
			}
			#endif
		}
		
		if(!frame_skip)
			stretch_blit(canvas, screen, 0, 0, 256, 240, canvas_x, 0, 256 * multiplier, 240 * multiplier);
		
		nes_mem.joy1 = GetJoyInput(joy_input_default);
		nes_mem.joy2 = 0;
		
		frame++;
	}
	
	if(log_file) fclose(log_file);
	
	if(wav_fn){
		SAMPLE *wav_smpl = create_sample(16, 0, 44100, (wav_buf_siz - 1) * audio_buf_siz);
		memcpy(wav_smpl->data, wav_buf, audio_buf_siz * (wav_buf_siz - 1) * 2);
		save_wav(wav_fn, wav_smpl);
		destroy_sample(wav_smpl);
		free(wav_buf);
	}
	
	allegro_exit();

	return 0;
}
END_OF_MAIN()
