#include "6502.h"
#include "readfile.h"
#include "apu.h"
#include "NESdefs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "NESMemory.h"

static uint8_t DMCRead(uint16_t addr, void *user_data)
{
	Memory *mem = (Memory *) user_data;
	uint8_t tmp = (*mem)[addr];
	return tmp;
}

void NESMemory::SyncAPU(bool sync_regs)
{
	if(sync_regs){
		memcpy(apu_state.regs, apu, sizeof(apu));
		memcpy(apu_state.reg_w, apu_w, sizeof(apu));
	}
	
	int nsamples = (apuCycles * 44100) / apu_state.clock;
	if(!nsamples){
		nsamples = 1;
	}
	nsamples = apu_sample(&apu_state, &ptrSampled, ptrSampled + nsamples, nsamples);
	apuCycles -= (nsamples * apu_state.clock) / 44100;
	
	memcpy(apu, apu_state.regs, sizeof(apu));
	memcpy(apu_w, apu_state.reg_w, sizeof(apu));
}

uint16_t *NESMemory::GetSamples(int n)
{
	uint16_t *ret;
	if(ptrBuffered + n > ptrSampled){
		return 0;
		memset(ptrSampled, 0, ((uint8_t *)(ptrBuffered + n)) - ((uint8_t *)ptrSampled));
		ptrSampled = ptrBuffered + n;
	}
	ret = ptrBuffered;
	ptrBuffered += n;
	
	if(ptrSampled - sampleBuffer > sizeof(sampleBuffer) / 4){
		memcpy(sampleBuffer, ptrBuffered, (ptrSampled - ptrBuffered) * 2);
		ptrSampled = sampleBuffer + (ptrSampled - ptrBuffered);
		ptrBuffered = sampleBuffer;
	}
	
	return ret;
}

int NESMemory::GetDMACycles()
{
	int temp = dma_cycles;
	dma_cycles = 0;
	return temp;
}

uint8_t &NESMemory::GetPPU(uint16_t addr)
{
	addr &= 0x3FFFu;
	uint16_t a10 = addr & bit10;
	uint16_t a11 = addr & bit11;
	
	switch(addr){
		case 0x0 ... 0x0FFF:
			if(mapper == 1){
				if(!(mmc_ctrl & bit4)){
					mmc_chr_8k:
					return chr[addr + 0x2000 * (mmc_chr_bnk0 >> 1)];
				}
				return chr[addr + 0x1000 * mmc_chr_bnk0];
			}
		case 0x1000 ... 0x1FFF:
			if(mapper == 1){
				if(!(mmc_ctrl & bit4)) goto mmc_chr_8k;
				addr -= 0x1000;
				return chr[addr + 0x1000 * mmc_chr_bnk1];
			}
			return chr[addr + 0x2000 * (cnrom_bank % (chr_siz ? chr_siz : 1))];
		case 0x2000 ... 0x3EFF:
			if(has_vram) return vram[addr & (bit12 - 1u)];
			/*
			|||||||+- Mirroring: 0: horizontal (vertical arrangement) (CIRAM A10 = PPU A11)
			|||||||              1: vertical (horizontal arrangement) (CIRAM A10 = PPU A10)
			*/
			if(mapper != 1) return vram[(addr & (bit10 - 1u)) | (mirror ? (addr & bit10) : ((addr & bit11) >> 1u))];
			if((mmc_ctrl & 3) == 0) return vram[(addr & (bit10 - 1u))];
			if((mmc_ctrl & 3) == 1) return vram[(addr & (bit10 - 1u)) | bit10];
			if((mmc_ctrl & 3) == 2) return vram[(addr & (bit10 - 1u)) | (addr & bit10)];
			if((mmc_ctrl & 3) == 3) return vram[(addr & (bit10 - 1u)) | ((addr & bit11) >> 1u)];
		case 0x3F00 ... 0x3FFF:
			//SMB sky fix
			if(addr == 0x3F10) addr = 0x3F00;
			return ppu_pal[addr & 0x1fu];
		default:
			fputs("Unmapped PPU address", stderr);
			abort();
	}
}

void NESMemory::Read(const MemoryByte &mb)
{
	if(mb.ptr == ppu + PPUSTATUS){
		ppu[PPUSTATUS] &= (255u - PPUSTATUS_VBLANK);
		ppu[PPUSTATUS] &= (255u - PPUSTATUS_SPR0HIT);
		ppu_latch = 0;
	}
	if(mb.ptr == apu + JOY1_PORT){
		if(joy_latch == 2){
			joy1_bit <<= 1u;
			//0x40 to simulate the openbus behaviour (from high byte of 0x40 which is last read before)
			apu[JOY1_PORT] = 0x40 | ((joy1 & joy1_bit) ? 1 : 0);
		}
	}
	if(mb.ptr == &joy2_port){
		if(joy_latch == 2){
			joy2_bit <<= 1u;
			//0x40 to simulate the openbus behaviour (from high byte of 0x40 which is last read before)
			joy2_port = 0x40 | ((joy2 & joy2_bit) ? 1 : 0);
		}
	}
	if(mb.ptr == ppu + PPUDATA){
		ppu[PPUDATA] = GetPPU(ppu_addr);
		
		if(ppu[PPUCTRL] & PPUCTRL_INC) ppu_addr += 32;
		else ppu_addr++;
	}
	if(mb.ptr == apu + APU_SND_CHN){
		//Clear frame interrupt
		apu_state.irq_frame = 0;
		apu_state.regs[APU_SND_CHN] &= ~bit6;
	}
}

void NESMemory::Write(const MemoryByte &mb)
{
	if(mb.ptr == ppu + OAMDATA){
		oam[ppu[OAMADDR]] = ppu[OAMDATA];
	}
	if(mb.ptr == ppu + PPUDATA){
		GetPPU(ppu_addr) = ppu[PPUDATA];
		if(ppu[PPUCTRL] & PPUCTRL_INC) ppu_addr += 32;
		else ppu_addr++;
	}
	if(mb.ptr == ppu + PPUADDR){
		if(!(ppu_latch & 1)){
			ppu_addr &= 255u;
			ppu_addr |= ppu[PPUADDR] << 8u;
			
			//Hack based on loopy's PPU doc
			ppu[PPUCTRL] &= 0xFFu - PPUCTRL_NAMETBL;
			ppu[PPUCTRL] |= (ppu[PPUADDR] >> 2u) & PPUCTRL_NAMETBL;
		}
		else{
			ppu_addr &= 255u << 8u;
			ppu_addr |= ppu[PPUADDR];
		}
		ppu_latch++;
	}
	if(mb.ptr == ppu + PPUSCROLL){
		if(!(ppu_latch & 1)){
			ppu_x = ppu[PPUSCROLL];
		}
		else{
			ppu_y = ppu[PPUSCROLL];
		}
		ppu_latch++;
	}
	if(mb.ptr == apu + OAMDMA){
		for(uint16_t addr = apu[OAMDMA] << 8u, count = 0; count < 256; addr++, count++){
			oam[count] = (*this)[addr];
		}
		dma_cycles += 514;
	}
	else if(mb.ptr == apu + JOY1_PORT){
		if(apu[JOY1_PORT] & 1) joy_latch = 1;
		else if((!(apu[JOY1_PORT] & 1)) && (joy_latch == 1)){
			joy_latch++;
			joy1_bit = 1;
			joy2_bit = 1;
			
			//0x40 to simulate the openbus behaviour (from high byte of 0x40 which is last read before)
			apu[JOY1_PORT] = 0x40 | ((joy1 & joy1_bit) ? 1 : 0);
			joy2_port = 0x40 | ((joy2 & joy2_bit) ? 1 : 0);
		}
	}
	if(mb.ptr >= apu && mb.ptr - apu < 0x18){
		apu_w[mb.ptr - apu] = 1;
	}
	if(mb.wptr == &cnrom_bank){
		//printf("Switched to CHR bank %d\n", cnrom_bank);
	}
	if(mb.wptr == &unrom_bank){
		//printf("Switched to CHR bank %d\n", cnrom_bank);
	}
	if(mb.wptr == &mmc_shift_bit){
		if(mmc_shift_bit & bit7){
			mmc_shift_reg = 0;
			mmc_shift_cnt = 0;
			mmc_ctrl |= 0xC;
			return;
		}
		
		mmc_shift_reg = mmc_shift_reg >> 1u;
		mmc_shift_reg |= (mmc_shift_bit & 1u) << 4u;
		mmc_shift_cnt++;
		if(mmc_shift_cnt == 5){
			mmc_shift_reg &= bit5 - 1u;
			switch(mb.addr){
				case 0x8000 ... 0x9FFF:
					mmc_ctrl = mmc_shift_reg;
					break;
				case 0xA000 ... 0xBFFF:
					mmc_chr_bnk0 = mmc_shift_reg;
					break;
				case 0xC000 ... 0xDFFF:
					mmc_chr_bnk1 = mmc_shift_reg;
					break;
				case 0xE000 ... 0xFFFF:
					mmc_prg_bank = mmc_shift_reg;
					break;
			}
			#if 0
			printf(
				"MMC %d %d %d %d - %04X\n",
				mmc_ctrl,
				mmc_chr_bnk0,
				mmc_chr_bnk1,
				mmc_prg_bank,
				mb.addr
			);
			#endif
			
			#define NEXTPOW2_1(x) ({\
				int _x = (x);\
				int _p = 1;\
				while(_p < _x) _p *= 2;\
				(_p - 1) ? (_p - 1) : 1;\
			})
			
			//mmc_chr_bnk0 &= 1;
			//mmc_chr_bnk1 &= 1;
			mmc_prg_bank &= (bit4 - 1u);
			
			mmc_prg_bank &= NEXTPOW2_1(prg_siz);
			
			//printf("$A000 - %02X %02X %02X\n", (uint8_t) (*this)[0xA000], (uint8_t) (*this)[0xA001], (uint8_t) (*this)[0xA002]);
			
			mmc_shift_cnt = 0;
			mmc_shift_reg = 0;
		}
	}
	if(mb.ptr >= sram && mb.ptr - sram < 0x2000){
		//printf("SRAM %04X %02X\n", mb.addr, sram[mb.addr - 0x2000]);
	}
}

MemoryByte NESMemory::operator[](std::size_t address)
{
	MemoryByte mb(this);
	mb.addr = address;
	mb.wptr = 0;
	switch(address){
		case 0x0 ... 0x1fff:
			mb.ptr = ram + (address & 0x7ff);
			break;
		case 0x2000 ... 0x3fff:
			mb.ptr = ppu  + (address & 7);
			break;
		case 0x4000 ... 0x4016:
			mb.ptr = apu + (address - 0x4000);
			SyncAPU();
			break;
		case 0x4017:
			mb.wptr = apu + (address - 0x4000);
			mb.ptr = &joy2_port;
			SyncAPU();
			break;
		case 0x6000 ... 0x7fff:
			mb.ptr = sram + (address - 0x6000);
			break;
		case 0x8000 ... 0xBFFF:
			mb.ptr = rom + (address - 0x8000) + (0x4000 * ((uint16_t) unrom_bank));
			break;
		case 0xC000 ... 0xFFFF:
			mb.ptr = rom + (address - 0xC000) + (0x4000 * (prg_siz - 1));
			break;
		default:
			fprintf(stderr, "Unmapped CPU address $%04X\n", address);
			open_bus = address >> 8u;
			mb.ptr = &open_bus;
			//abort();
			break;
	}
	switch(address){
		case 0x8000 ... 0xFFFF:
			if(mapper == 1)	mb.wptr = &mmc_shift_bit;
			if(mapper == 2) mb.wptr = &unrom_bank;
			if(mapper == 3) mb.wptr = &cnrom_bank;
		default:
			break;
	}
	if(mapper == 1){
		switch(address){
			case 0x8000 ... 0xBFFF:
				if(((mmc_ctrl >> 2) & 3) < 2){
					mmc_prg_32k:
					mb.ptr = rom + (address - 0x8000) + (0x4000 * (mmc_prg_bank >> 1));
				}
				if(((mmc_ctrl >> 2) & 3) == 2)
					mb.ptr = rom + (address - 0x8000);
				if(((mmc_ctrl >> 2) & 3) == 3)
					mb.ptr = rom + (address - 0x8000) + (0x4000 * ((uint16_t) mmc_prg_bank));
				break;
			case 0xC000 ... 0xFFFF:
				if(((mmc_ctrl >> 2) & 3) < 2) goto mmc_prg_32k;
				if(((mmc_ctrl >> 2) & 3) == 3)
					mb.ptr = rom + (address - 0xC000) + (0x4000 * (prg_siz - 1));
				if(((mmc_ctrl >> 2) & 3) == 2)
					mb.ptr = rom + (address - 0xC000) + (0x4000 * (mmc_prg_bank));
				break;
			default:
				break;
		}
		if(address > 0x8000 && address < 0xFFFF && mb.ptr > rom + 0x4000 * prg_siz){
			fprintf(stderr, "%04X\tMMC1 Error - %d", address, mmc_prg_bank);
			abort();
		}
	}
	return mb;
}

void NESMemory::Savestate(FILE *f)
{
	SyncAPU();
	fwrite(ram, 1, sizeof(ram), f);
	fwrite(ppu, 1, sizeof(ppu), f);
	fwrite(apu, 1, sizeof(apu), f);
	fwrite(apu_w, 1, sizeof(apu_w), f);
	fwrite(sram, 1, 0x2000, f);
	fwrite(oam, 1, 0x100, f);
	fwrite(vram, 1, 0x1000, f);
	fwrite(ppu_pal, 1, 0x20, f);
	fwrite(&ppu_addr, 1, sizeof(ppu_addr), f);
	fwrite(&ppu_x, 1, sizeof(ppu_x), f);
	fwrite(&ppu_y, 1, sizeof(ppu_y), f);
	fwrite(&ppu_latch, 1, sizeof(ppu_latch), f);
	fwrite(&joy1_bit, 1, 1, f);
	fwrite(&joy2_bit, 1, 1, f);
	fwrite(&joy_latch, 1, 1, f);
	fwrite(&unrom_bank, 1, 1, f);
	fwrite(chr_ram, 1, 16384, f);
	fwrite(&cnrom_bank, 1, 1, f);
	fwrite(&mmc_shift_bit, 1, 1, f);
	fwrite(&mmc_shift_reg, 1, 1, f);
	fwrite(&mmc_shift_cnt, 1, 1, f);
	fwrite(&mmc_ctrl, 1, 1, f);
	fwrite(&mmc_chr_bnk0, 1, 1, f);
	fwrite(&mmc_chr_bnk1, 1, 1, f);
	fwrite(&mmc_prg_bank, 1, 1, f);
	fwrite(&dma_cycles, 1, sizeof(int), f);
	fwrite(&mirror, 1, sizeof(int), f);
	fwrite(&apu_state, 1, sizeof(apu_state), f);
	fwrite(&apuCycles, 1, sizeof(int), f);
}

void NESMemory::Loadstate(FILE *f)
{
	fread(ram, 1, sizeof(ram), f);
	fread(ppu, 1, sizeof(ppu), f);
	fread(apu, 1, sizeof(apu), f);
	fread(apu_w, 1, sizeof(apu_w), f);
	fread(sram, 1, 0x2000, f);
	fread(oam, 1, 0x100, f);
	fread(vram, 1, 0x1000, f);
	fread(ppu_pal, 1, 0x20, f);
	fread(&ppu_addr, 1, sizeof(ppu_addr), f);
	fread(&ppu_x, 1, sizeof(ppu_x), f);
	fread(&ppu_y, 1, sizeof(ppu_y), f);
	fread(&ppu_latch, 1, sizeof(ppu_latch), f);
	fread(&joy1_bit, 1, 1, f);
	fread(&joy2_bit, 1, 1, f);
	fread(&joy_latch, 1, 1, f);
	fread(&unrom_bank, 1, 1, f);
	fread(chr_ram, 1, 16384, f);
	fread(&cnrom_bank, 1, 1, f);
	fread(&mmc_shift_bit, 1, 1, f);
	fread(&mmc_shift_reg, 1, 1, f);
	fread(&mmc_shift_cnt, 1, 1, f);
	fread(&mmc_ctrl, 1, 1, f);
	fread(&mmc_chr_bnk0, 1, 1, f);
	fread(&mmc_chr_bnk1, 1, 1, f);
	fread(&mmc_prg_bank, 1, 1, f);
	fread(&dma_cycles, 1, sizeof(int), f);
	fread(&mirror, 1, sizeof(int), f);
	fread(&apu_state, 1, sizeof(apu_state), f);
	fread(&apuCycles, 1, sizeof(int), f);
	apu_state.dmc_read_user_data = this;
	apu_state.dmc_read = DMCRead;
}

extern "C"{
int exists(const char *filename);
}

void NESMemory::LoadROM(const char *fn)
{
	ines_len = read_file(fn, (char **) &ines, 1);
	if((!ines_len) || (!ines)){
		fputs("couldn't read file", stderr);
		abort();
	}
	if(memcmp(ines, "NES\x1a", 4)){
		fputs("Not a NES ROM", stderr);
		abort();
	}
	prg_siz = ines[4];
	chr_siz = ines[5];
	mirror = ines[6] & 1;
	has_sram = ines[6] & 2;
	has_trainer = ines[6] & 4;
	has_vram = ines[6] & 8;
	mapper = (ines[6] >> 4) | (ines[7] & 240);
	switch(mapper){
		case 0:
			break;
		case 2:
			//printf("%d banks.\n", prg_siz);
			break;
		case 3:
			//printf("%d CHR banks.\n", chr_siz);
			break;
		case 1:
			printf("%d PRG banks, %d CHR banks\n", prg_siz, chr_siz);
			break;
		default:
			fprintf(stderr, "Mapper %d not supported", mapper);
			abort();
	}
	if(has_trainer){
		fputs("Fuck trainer", stderr);
		abort();
	}
	rom = ines + 16;
	chr = rom + 16384 * prg_siz;
	if(!chr_siz){
		chr = chr_ram;
	}
	
	if(has_sram){
		char *srfptr;
		strcpy(battery_fn, fn);
		srfptr = battery_fn + strlen(battery_fn);
		while(*srfptr != '.') srfptr--;
		srfptr++; *srfptr = 'b';
		srfptr++; *srfptr = 'a';
		srfptr++; *srfptr = 't';
		if(exists(battery_fn)){
			FILE *battery_file = fopen(battery_fn, "rb");
			fread(sram, 1, 0x2000, battery_file);
			fclose(battery_file);
		}
	}
}

NESMemory::NESMemory() : 
	rom(0),
	ines(0),
	chr(0),
	ines_len(0),
	prg_siz(0),
	chr_siz(0),
	mirror(0),
	has_sram(0),
	has_trainer(0),
	has_vram(0),
	mapper(0),
	ppu_addr(0),
	dma_cycles(0),
	ppu{},
	apu{},
	ppu_pal{},
	ppu_x(0), ppu_y(0), ppu_latch(0), joy1(0), joy2(0), joy1_bit(0), joy2_bit(0), joy_latch(0),
	ram{},
	unrom_bank(0),
	apu_state{},
	apu_w{},
	apuCycles(0),
	sampleBuffer{},
	ptrBuffered(sampleBuffer),
	ptrSampled(sampleBuffer),
	cnrom_bank(0),
	mmc_shift_bit(0),
	mmc_shift_reg(0),
	mmc_shift_cnt(0),
	mmc_ctrl(3 << 2),
	mmc_chr_bnk0(0),
	mmc_chr_bnk1(0),
	mmc_prg_bank(0),
	battery_fn{}
{
	for(unsigned int i = 0; i < sizeof(ram); i++) ram[i] = (uint8_t) rand();
	for(unsigned int i = 0; i < sizeof(chr_ram); i++) chr_ram[i] = (uint8_t) rand();
	
	apu_state.clock = 1789773;
	apu_state.dmc_read_user_data = this;
	apu_state.dmc_read = DMCRead;
}

NESMemory::~NESMemory()
{
	if(ines) free(ines);
	if(has_sram){
		FILE *battery_file = fopen(battery_fn, "wb");
		fwrite(sram, 1, 0x2000, battery_file);
		fclose(battery_file);
	}
}
