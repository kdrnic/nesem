enum PPUReg
{
	PPUCTRL,   //$2000
	PPUMASK,   //$2001
	PPUSTATUS, //$2002
	OAMADDR,   //$2003
	OAMDATA,   //$2004
	PPUSCROLL, //$2005
	PPUADDR,   //$2006
	PPUDATA    //$2007
};

#define JOY2_PORT APU_FRAME

#define JOY_BUTTON_A		bit0
#define JOY_BUTTON_B		bit1
#define JOY_BUTTON_SELECT	bit2
#define JOY_BUTTON_START	bit3
#define JOY_BUTTON_UP		bit4
#define JOY_BUTTON_DOWN		bit5
#define JOY_BUTTON_LEFT		bit6
#define JOY_BUTTON_RIGHT	bit7

#ifndef bit0
#define bit0  0x1u
#define bit1  0x2u
#define bit2  0x4u
#define bit3  0x8u
#define bit4  0x10u
#define bit5  0x20u
#define bit6  0x40u
#define bit7  0x80u
#define bit8  0x100u
#define bit9  0x200u
#define bit10 0x400u
#define bit11 0x800u
#define bit12 0x1000u
#define bit13 0x2000u
#define bit14 0x4000u
#define bit15 0x8000u
#endif

#define PPUCTRL_NAMETBL (bit0 | bit1)
#define PPUCTRL_INC (bit2)
#define PPUCTRL_SPRADDR (bit3)
#define PPUCTRL_BGADDR (bit4)
#define PPUCTRL_SPRSIZ (bit5)
#define PPUCTRL_SLAVE (bit6)
#define PPUCTRL_NMI (bit7)

/*
|||| |||+- Greyscale (0: normal color, 1: produce a greyscale display)
|||| ||+-- 1: Show background in leftmost 8 pixels of screen, 0: Hide
|||| |+--- 1: Show sprites in leftmost 8 pixels of screen, 0: Hide
|||| +---- 1: Show background
|||+------ 1: Show sprites
||+------- Emphasize red
|+-------- Emphasize green
+--------- Emphasize blue
*/
#define PPUMASK_GREYSCL bit0
#define PPUMASK_BORDERBG bit1
#define PPUMASK_BORDERSPR bit2
#define PPUMASK_SHOWBG bit3
#define PPUMASK_SHOWSPR bit4
#define PPUMASK_R bit5
#define PPUMASK_G bit6
#define PPUMASK_B bit7

#define PPUSTATUS_SPROVF (bit5)
#define PPUSTATUS_SPR0HIT (bit6)
#define PPUSTATUS_VBLANK (bit7)
