#include <allegro.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "6502.h"
#include "apu.h"
#include "NESdefs.h"
#include "NESMemory.h"
#include "Render.h"

static const uint8_t nes_rgb[3 * 64] =
{84, 84, 84,   0, 30,116,   8, 16,144,  48,  0,136,  68,  0,100,  92,  0, 48,  84,  4,  0,  60, 24,  0,  32, 42,  0,   8, 58,  0,   0, 64,  0,   0, 60,  0,   0, 50, 60,   0,  0,  0, 0,0,0, 0,0,0,
152,150,152,   8, 76,196,  48, 50,236,  92, 30,228, 136, 20,176, 160, 20,100, 152, 34, 32, 120, 60,  0,  84, 90,  0,  40,114,  0,   8,124,  0,   0,118, 40,   0,102,120,   0,  0,  0, 0,0,0, 0,0,0,
236,238,236,  76,154,236, 120,124,236, 176, 98,236, 228, 84,236, 236, 88,180, 236,106,100, 212,136, 32, 160,170,  0, 116,196,  0,  76,208, 32,  56,204,108,  56,180,204,  60, 60, 60, 0,0,0, 0,0,0,
236,238,236, 168,204,236, 188,188,236, 212,178,236, 236,174,236, 236,174,212, 236,180,176, 228,196,144, 204,210,120, 180,222,120, 168,226,144, 152,226,180, 160,214,228, 160,162,160, 0,0,0, 0,0,0};

static uint8_t bitswap(uint8_t b)
{
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

void RenderScanline(NESMemory &nes_mem, uint8_t scr_y, BITMAP *bmp)
{
	unsigned int y = nes_mem.ppu_y + scr_y;
	for(unsigned int scr_x = 0, x = nes_mem.ppu_x; scr_x < 256; scr_x++, x++){
		const unsigned int coarse_x_lo = (x >> 3u) & (bit5 - 1u);
		const unsigned int coarse_y_lo = ((y >> 3u) % 30);
		const unsigned int coarse_x_hi = (x >> 3u) >= 32u; // (x >> 3u) >= 32;
		const unsigned int coarse_y_hi = (y >> 3u) >= 30u;
		const unsigned int fine_x = x & (bit3 - 1u);
		const unsigned int fine_y = y & (bit3 - 1u);
		const unsigned int nametbl = nes_mem.ppu[PPUCTRL] & PPUCTRL_NAMETBL;
		/*
		432 10 98765 43210
		yyy NN YYYYY XXXXX
		||| || ||||| +++++ coarse X scroll
		||| || +++++------ coarse Y scroll
		||| ++------------ nametable select
		+++--------------- fine Y scroll ---- KDRNIC: what????
		*/
		
		unsigned int bg_name_addr =
			(coarse_x_lo) |
			(coarse_y_lo << 5u) |
			((nametbl << 10u) ^ ((coarse_x_hi << 10u) | (coarse_y_hi << 11u))) |
			bit13;
		
		//if(!scr_y && !scr_x) printf("%04X %X %X\n", bg_name_addr, nametbl, coarse_x_hi | (coarse_y_hi << 1u));
		
		const unsigned int bg_name = nes_mem.GetPPU(bg_name_addr);
		
		/*
		|||||| |||||+++ T: Fine Y offset, the row number within a tile
		|||||| ||||+--- P: Bit plane (0: "lower"; 1: "upper")
		|||||| ++++---- C: Tile column
		||++++--------- R: Tile row
		|+------------- H: Half of sprite table (0: "left"; 1: "right")
		+-------------- 0: Pattern table is at $0000-$1FFF
		*/
		const unsigned int bg_table = (nes_mem.ppu[PPUCTRL] & PPUCTRL_BGADDR) >> 4u;
		const unsigned int bg_plane0_addr =
			fine_y |
			0 |
			(bg_name << 4u) |
			(bg_table << 12u);
		const unsigned int bg_plane1_addr =
			fine_y |
			bit3 |
			(bg_name << 4u) |
			(bg_table << 12u);
		
		unsigned int bg_plane0 = nes_mem.GetPPU(bg_plane0_addr);
		unsigned int bg_plane1 = nes_mem.GetPPU(bg_plane1_addr);
		
		bg_plane0 = bitswap(bg_plane0);
		bg_plane1 = bitswap(bg_plane1);
		
		unsigned int bg_idx = 
			((bg_plane0 & (1u << fine_x)) ? 1u : 0) |
			((bg_plane1 & (1u << fine_x)) ? 2u : 0);
		
		const unsigned int bg_attr_addr =
			((bg_name_addr & (bit10 | bit11 | bit13)) +
			0x3C0u) +
			(coarse_x_lo >> 2u) +
			((coarse_y_lo >> 2u) * 8);
		
		const unsigned int bg_attr = nes_mem.GetPPU(bg_attr_addr);
		
		unsigned int bg_pal = 0;
		/*
		7654 3210
		|||| ||++ Color bits 3-2 for top left quadrant of this byte
		|||| ++-- Color bits 3-2 for top right quadrant of this byte
		||++----- Color bits 3-2 for bottom left quadrant of this byte
		++------- Color bits 3-2 for bottom right quadrant of this byte
		*/
		if( (coarse_x_lo & 2) &&  (coarse_y_lo & 2)) bg_pal = bg_attr >> 6u;
		if(!(coarse_x_lo & 2) &&  (coarse_y_lo & 2)) bg_pal = bg_attr >> 4u;
		if( (coarse_x_lo & 2) && !(coarse_y_lo & 2)) bg_pal = bg_attr >> 2u;
		if(!(coarse_x_lo & 2) && !(coarse_y_lo & 2)) bg_pal = bg_attr;
		bg_pal &= 3u;
		
		if(
			((!(nes_mem.ppu[PPUMASK] & PPUMASK_BORDERBG)) && (scr_x < 8))
			|| (!(nes_mem.ppu[PPUMASK] & PPUMASK_SHOWBG))
		){
			bg_idx = 0;
		}
		
		unsigned int bg_colour = nes_mem.GetPPU(0x3f00 | (bg_pal << 2u) | bg_idx);
		if(!bg_idx) bg_colour = nes_mem.GetPPU(0x3f00);
		
		unsigned int nes_colour = bg_colour;
		
		if(!(
			((!(nes_mem.ppu[PPUMASK] & PPUMASK_BORDERSPR)) && (scr_x < 8))
			|| (!(nes_mem.ppu[PPUMASK] & PPUMASK_SHOWSPR))
		)) for(unsigned int sp_n = 0; sp_n < 64; sp_n++){
			const unsigned int sp_y =    nes_mem.oam[sp_n * 4 + 0] + 1;
			      unsigned int sp_name = nes_mem.oam[sp_n * 4 + 1];
			const unsigned int sp_attr = nes_mem.oam[sp_n * 4 + 2];
			const unsigned int sp_x =    nes_mem.oam[sp_n * 4 + 3];
			
			/*
			byte 2
			76543210
			||||||||
			||||||++ Palette (4 to 7) of sprite
			|||+++-- Unimplemented
			||+----- Priority (0: in front of background; 1: behind background)
			|+------ Flip sprite horizontally
			+------- Flip sprite vertically
			*/
			
			if(sp_y > scr_y) continue;
			if(sp_x > scr_x) continue;
			if(sp_y + ((nes_mem.ppu[PPUCTRL] & PPUCTRL_SPRSIZ) ? 15 : 7) < scr_y) continue;
			if(sp_x + 7 < scr_x) continue;
			
			unsigned int sp_table = (nes_mem.ppu[PPUCTRL] & PPUCTRL_SPRADDR) >> 3u;
			
			if(nes_mem.ppu[PPUCTRL] & PPUCTRL_SPRSIZ){
				sp_table = sp_name & 1u;
				sp_name &= 0xFFu - 1u;
			}
			
			unsigned int sp_line = scr_y - sp_y;
			if(sp_line >= 8){
				if(!(sp_attr & bit7)){
					sp_name = (sp_name + 1u) & 0xFFu;
				}
				sp_line -= 8;
			}
			else if((sp_attr & bit7) && (nes_mem.ppu[PPUCTRL] & PPUCTRL_SPRSIZ)){
				sp_name = (sp_name + 1u) & 0xFFu;
			}
			if(sp_attr & bit7) sp_line = ~sp_line;
			sp_line &= bit3 - 1u;
			
			const unsigned int sp_plane0_addr =
				sp_line |
				0 |
				(sp_name << 4u) |
				(sp_table << 12u);
			const unsigned int sp_plane1_addr =
				sp_line |
				bit3 |
				(sp_name << 4u) |
				(sp_table << 12u);
			
			unsigned int sp_plane0 = nes_mem.GetPPU(sp_plane0_addr); 
			unsigned int sp_plane1 = nes_mem.GetPPU(sp_plane1_addr);
			
			if(!(sp_attr & bit6)){
				sp_plane0 = bitswap(sp_plane0);
				sp_plane1 = bitswap(sp_plane1);
			}
			
			unsigned int sp_idx = 
				((sp_plane0 & (1u << ((scr_x - sp_x) & (bit3 - 1u)) )) ? 1 : 0) |
				((sp_plane1 & (1u << ((scr_x - sp_x) & (bit3 - 1u)) )) ? 2 : 0);
			
			const unsigned int sp_pal = sp_attr & (bit2 - 1u);
			
			const unsigned int sp_colour = nes_mem.GetPPU(0x3f10 | (sp_pal << 2u) | sp_idx);
			if(sp_idx){
				if(bg_idx && sp_n == 0){
					nes_mem.ppu[PPUSTATUS] |= PPUSTATUS_SPR0HIT;
				}
				
				if((!(sp_attr & bit5)) || (!bg_idx)){
					nes_colour = sp_colour;
				}
				break;
			}
		}
		
		if(nes_mem.ppu[PPUMASK] & PPUMASK_GREYSCL) nes_colour &= 0x30;
		
		unsigned int bmp_r = nes_rgb[(nes_colour & 63u) * 3u + 0u];
		unsigned int bmp_g = nes_rgb[(nes_colour & 63u) * 3u + 1u];
		unsigned int bmp_b = nes_rgb[(nes_colour & 63u) * 3u + 2u];
		
		if(nes_mem.ppu[PPUMASK] & PPUMASK_R){
			bmp_r += bmp_r / 4;
			bmp_g -= bmp_g / 4;
			bmp_b -= bmp_b / 4;
		}
		if(nes_mem.ppu[PPUMASK] & PPUMASK_G){
			bmp_r -= bmp_r / 4;
			bmp_g += bmp_g / 4;
			bmp_b -= bmp_b / 4;
		}
		if(nes_mem.ppu[PPUMASK] & PPUMASK_B){
			bmp_r -= bmp_r / 4;
			bmp_g -= bmp_g / 4;
			bmp_b += bmp_b / 4;
		}
		bmp_r = MIN(255, MAX(0, bmp_r));
		bmp_g = MIN(255, MAX(0, bmp_g));
		bmp_b = MIN(255, MAX(0, bmp_b));
		
		if(bmp){
			const unsigned int bmp_colour = makecol(bmp_r, bmp_g, bmp_b);
			putpixel(bmp, scr_x, scr_y, bmp_colour);
		}
	}
}

void CreateNESPalette(PALETTE pal)
{
	for(int r = 0, g = 1, b = 2, i = 0, idx = PAL_OFFSET; i < 64; i++, idx++, r+=3, g+=3, b+=3){
		pal[idx].r = nes_rgb[r] >> 2;
		pal[idx].g = nes_rgb[g] >> 2;
		pal[idx].b = nes_rgb[b] >> 2;
	}
	for(int i = 0, idx = 128; i < 64; i++, idx++){
		pal[idx].r = ((i >> 0) & 3) << 4;
		pal[idx].g = ((i >> 2) & 3) << 4;
		pal[idx].b = ((i >> 4) & 3) << 4;
	}
	pal[255].r = 0xFF;
	pal[255].g = 0xFF;
	pal[255].b = 0xFF;
}
