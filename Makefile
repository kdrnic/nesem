ifeq ($(filter debug,$(MAKECMDGOALS)),debug)
	OBJDIR:=$(OBJDIR)dbg
	CFLAGS+=-g -DDEBUG
	BINNAME:=$(BINNAME)_dbg
endif

ifeq ($(filter profile,$(MAKECMDGOALS)),profile)
	CFLAGS+=-gdwarf-2 -fno-omit-frame-pointer
endif

ifeq ($(filter regular,$(MAKECMDGOALS)),regular)
	CFLAGS+=-O3
endif

OBJDIR=obj
CFLAGS2=$(CFLAGS)
CFLAGS2+=-Wall -Wuninitialized -Werror=implicit-function-declaration -Wno-unused
CPPFLAGS2=$(CFLAGS2) -Wno-reorder

BINNAME=game$(BINSUFFIX)

ifeq ($(origin CC),default)
	ifeq ($(OS),Windows_NT)
		CC = gcc
		CXX = g++
	endif
endif

ifeq ($(CC),clang)
	ifeq ($(OS),Windows_NT)
		CFLAGS2+=-target i686-pc-windows-gnu
	endif
endif

CPP_FILES=$(wildcard src/*.cpp)
C_FILES=$(wildcard src/*.c)
CPP_OBJECTS=$(patsubst src/%,$(OBJDIR)/%,$(patsubst %.cpp,%.o,$(CPP_FILES)))
C_OBJECTS=$(patsubst src/%,$(OBJDIR)/%,$(patsubst %.c,%.o,$(C_FILES)))
OBJECTS=$(CPP_OBJECTS) $(C_OBJECTS)

HAVE_LIBS=

ifeq ($(STATIC_LIBGCC),1)
	LINK_FLAGS=-static -static-libgcc
else
	LINK_FLAGS=
endif

INCLUDE_PATHS=
LINK_PATHS=

ifeq ($(OS),Windows_NT)
	INCLUDE_PATHS+=-I$(ALLEGRO_PATH)include
	LINK_PATHS+=-L$(ALLEGRO_PATH)lib
endif

#ifeq ($(OS),Windows_NT)
#	ifneq ($(NO_PTHREADS),1)
#		INCLUDE_PATHS+=-I$(PTHW32_PATH)include
#		LINK_PATHS+=-L$(PTHW32_PATH)lib
#		HAVE_LIBS+=-lpthreadGC2
#	else
#		CFLAGS2+=-DNO_PTHREADS
#	endif
#endif

HEADER_FILES=$(wildcard src/*.h)

$(OBJDIR)/%.o: src/%.cpp $(HEADER_FILES)
	$(CXX) $(INCLUDE_PATHS) $(CPPFLAGS2) $< -c -o $@

$(OBJDIR)/%.o: src/%.c $(HEADER_FILES)
	$(CC) $(INCLUDE_PATHS) $(CFLAGS2) $< -c -o $@

regular: $(BINNAME).exe

clean:
	rm -f $(OBJDIR)/*.o
	rm -f $(BINDIR)*.exe

$(OBJDIR):
	mkdir $(OBJDIR)

$(BINNAME).exe: $(OBJDIR) $(OBJECTS)
	$(CXX) $(LINK_PATHS) $(LINK_FLAGS) $(CPPFLAGS2) $(OBJECTS) -o $(BINNAME).exe $(HAVE_LIBS) -lalleg -lm

debug: $(BINNAME).exe
profile: clean $(BINNAME).exe

